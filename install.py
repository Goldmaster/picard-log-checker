PLUGIN_NAME = "Picard Log Checker"
PLUGIN_AUTHOR = "Goldmaster"
PLUGIN_DESCRIPTION = "A MusicBrainz Picard plugin that will check log files for ripped CDs, to make sure music files are ripped correctly and displayed inside Picard."
PLUGIN_VERSION = '0.1'
PLUGIN_API_VERSIONS = ['2.2']
PLUGIN_LICENSE = "GPL-2.0-or-later"
PLUGIN_LICENSE_URL = "https://www.gnu.org/licenses/gpl-2.0.html"