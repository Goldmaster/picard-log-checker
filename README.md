# Picard Log Checker

A simple MusicBrainz Picard plugin that can be installed and will search for .log files and will show in a collum the quality according to [Hey Bro Check Log](https://github.com/ligh7s/hey-bro-check-log).

**Please note that this is in alpha. Unfortunately, I'm not a programmer, just the ideas man. So any help will be appreciated.**
